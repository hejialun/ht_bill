/*
 Navicat Premium Data Transfer

 Source Server         : 本机
 Source Server Type    : MySQL
 Source Server Version : 50733
 Source Host           : localhost:3306
 Source Schema         : ht_bill

 Target Server Type    : MySQL
 Target Server Version : 50733
 File Encoding         : 65001

 Date: 14/11/2024 09:35:54
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for bill_clazz
-- ----------------------------
DROP TABLE IF EXISTS `bill_clazz`;
CREATE TABLE `bill_clazz`  (
  `id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '账单分类表组件',
  `name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '分类名称',
  `icon` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '分类图标',
  `sort` int(11) NULL DEFAULT NULL COMMENT '排序',
  `create_date` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `create_user` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `update_date` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `update_user` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `del_flag` int(1) NULL DEFAULT NULL COMMENT '逻辑删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '消费分类表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of bill_clazz
-- ----------------------------
INSERT INTO `bill_clazz` VALUES ('1', '餐饮', '/icon/餐饮.png', 1, '2022-03-17 15:58:13', '1', NULL, NULL, 0);
INSERT INTO `bill_clazz` VALUES ('2', '交通', '/icon/交通.png', 2, '2022-03-17 15:58:13', '1', NULL, NULL, 0);
INSERT INTO `bill_clazz` VALUES ('3', '日用', '/icon/生活日用.png', 3, '2022-03-17 15:58:13', '1', NULL, NULL, 0);
INSERT INTO `bill_clazz` VALUES ('4', '购物', '/icon/购物.png', 4, '2022-03-17 15:58:13', '1', NULL, NULL, 0);
INSERT INTO `bill_clazz` VALUES ('5', '住房', '/icon/住房.png', 5, '2022-03-17 15:58:13', '1', NULL, NULL, 0);
INSERT INTO `bill_clazz` VALUES ('6', '教育', '/icon/教育.png', 6, '2022-03-17 15:58:13', '1', NULL, NULL, 0);
INSERT INTO `bill_clazz` VALUES ('7', '医疗', '/icon/医疗.png', 7, '2022-03-17 15:58:13', '1', NULL, NULL, 0);
INSERT INTO `bill_clazz` VALUES ('8', '其他', '/icon/其他.png', 8, '2022-03-17 15:58:13', '1', NULL, NULL, 0);

-- ----------------------------
-- Table structure for bill_record
-- ----------------------------
DROP TABLE IF EXISTS `bill_record`;
CREATE TABLE `bill_record`  (
  `id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '账单分类表组件',
  `bill_clazz` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '消费分类',
  `money` decimal(11, 2) NULL DEFAULT NULL COMMENT '消费金额',
  `bill_date` date NULL DEFAULT NULL COMMENT '消费日期',
  `note` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '消费备注',
  `create_date` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `create_user` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `update_date` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `update_user` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '消费记录表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of bill_record
-- ----------------------------
INSERT INTO `bill_record` VALUES ('5c8df5522ebdc8771b94064a87845ccd', '5', 20.00, '2024-11-13', '123', '2024-11-13 15:00:51', '1510bb904bfd780beb4a332064feed32', NULL, NULL);
INSERT INTO `bill_record` VALUES ('60707b1ed39d4dd6c0a5c5574c53cf1e', '1', 20.00, '2024-11-13', '还好', '2024-11-13 15:13:37', '1510bb904bfd780beb4a332064feed32', NULL, NULL);
INSERT INTO `bill_record` VALUES ('faecf3e9f66100dee0b2142ca6e3eb63', '1', 30.00, '2024-11-13', '123', '2024-11-13 15:01:00', '1510bb904bfd780beb4a332064feed32', NULL, NULL);

-- ----------------------------
-- Table structure for persistent_logins
-- ----------------------------
DROP TABLE IF EXISTS `persistent_logins`;
CREATE TABLE `persistent_logins`  (
  `username` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `series` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `token` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `last_used` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`series`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sys_dict
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict`;
CREATE TABLE `sys_dict`  (
  `id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  `dict_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '字典名称',
  `dict_code` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '字典编码',
  `desc` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '描述',
  `create_date` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `create_user` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `update_date` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `update_user` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `dict_code`(`dict_code`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '字典表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dict
-- ----------------------------
INSERT INTO `sys_dict` VALUES ('c149ee731afc80fd3891e97dd0627aba', '菜单类型', 'menu_type', '菜单类型', '2021-09-17 16:32:24', '1', NULL, NULL);
INSERT INTO `sys_dict` VALUES ('fdfc713e8f3c1e612c321c2a36858593', '状态', 'state', '字段状态：01:启用：02：禁用  -关联码表state', '2021-09-14 10:58:07', '1', NULL, NULL);

-- ----------------------------
-- Table structure for sys_dict_item
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_item`;
CREATE TABLE `sys_dict_item`  (
  `id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `dict_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '字典id',
  `label` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '字典项文本',
  `value` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '字典项值',
  `desc` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '描述',
  `sort` int(11) NULL DEFAULT NULL COMMENT '排序',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `index_table_dict_id`(`dict_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '字典项表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dict_item
-- ----------------------------
INSERT INTO `sys_dict_item` VALUES ('54bf12064d034a47ec675e931e2e2974', 'c149ee731afc80fd3891e97dd0627aba', '菜单', '0', '菜单', 0);
INSERT INTO `sys_dict_item` VALUES ('7d9f822b6e3aa128471309cd8b0a71e0', 'fdfc713e8f3c1e612c321c2a36858593', '启用', '01', '启用', 0);
INSERT INTO `sys_dict_item` VALUES ('af1d995b32e942c318bdb3d6b5bf0ad5', 'c149ee731afc80fd3891e97dd0627aba', '按钮', '2', '按钮', 2);
INSERT INTO `sys_dict_item` VALUES ('b6eece188b759c68def3708ede4a33ad', 'c149ee731afc80fd3891e97dd0627aba', '页面', '1', '页面', 0);
INSERT INTO `sys_dict_item` VALUES ('ef7669b086f140325231c0b0622db167', 'fdfc713e8f3c1e612c321c2a36858593', '禁用', '02', '禁用', 1);

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu`  (
  `id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '菜单id',
  `pid` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '上级菜单',
  `name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '菜单名',
  `icon` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图标编码',
  `code` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '编码',
  `path` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '菜单路径',
  `type` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '菜单类型：0：菜单：1：页面：2：按钮',
  `sort` int(11) NULL DEFAULT NULL COMMENT '排序',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '菜单表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES ('0d237ca74ad890c300a224902d1ae880', '1', '字典项管理', 'el-icon-menu', 'SYS_DICT', '/sys/dict/list', '1', 4);
INSERT INTO `sys_menu` VALUES ('1', '0', '系统管理', 'el-icon-s-tools', 'SYS', '', '0', 1);
INSERT INTO `sys_menu` VALUES ('10', '1', '菜单管理', 'el-icon-s-grid', 'SYS_MENU', '/sys/menu/list', '1', 3);
INSERT INTO `sys_menu` VALUES ('11', '10', '菜单编辑', 'el-icon-edit', 'SYS_MENU_UPDATE', '', '2', 1);
INSERT INTO `sys_menu` VALUES ('12', '10', '菜单新增', 'el-icon-plus', 'SYS_MENU_ADD', '', '2', 2);
INSERT INTO `sys_menu` VALUES ('13', '10', '菜单删除', 'el-icon-delete', 'SYS_MENU_DEL', '', '2', 3);
INSERT INTO `sys_menu` VALUES ('2', '1', '用户管理', 'el-icon-user-solid', 'SYS_USER', '/sys/user/list', '1', 1);
INSERT INTO `sys_menu` VALUES ('3', '2', '用户编辑', 'el-icon-edit', 'SYS_USER_UPDATE', '', '2', 1);
INSERT INTO `sys_menu` VALUES ('3131b3aeb78f2c9f29009c23b1afe963', '2', '重置密码', 'el-icon-refresh-right', 'SYS_USER_RESET_PASSWORD', '', '2', 4);
INSERT INTO `sys_menu` VALUES ('4', '2', '用户新增', 'el-icon-plus', 'SYS_USER_ADD', '', '2', 2);
INSERT INTO `sys_menu` VALUES ('4c6e74acf214c843a0526bbc566bedd8', '0d237ca74ad890c300a224902d1ae880', '字典新增', 'el-icon-plus', 'SYS_DICT_ADD', '', '2', 1);
INSERT INTO `sys_menu` VALUES ('5', '2', '用户删除', 'el-icon-delete', 'SYS_USER_DEL', '', '2', 3);
INSERT INTO `sys_menu` VALUES ('5e2db6fa3eebbad27de5eaf97e7430ba', '0d237ca74ad890c300a224902d1ae880', '字典项编辑', 'el-icon-edit', 'SYS_DICT_ITEM_UPDATE', '', '2', 5);
INSERT INTO `sys_menu` VALUES ('6', '1', '角色管理', 'el-icon-user', 'SYS_ROLE', '/sys/role/list', '1', 2);
INSERT INTO `sys_menu` VALUES ('6a894f406313aaad9be5745b26d9027c', '2', '用户状态修改', 'el-icon-edit', 'SYS_USER_UPDATE_STATE', '', '2', 6);
INSERT INTO `sys_menu` VALUES ('7', '6', '角色编辑', 'el-icon-edit', 'SYS_ROLE_UPDATE', '', '2', 1);
INSERT INTO `sys_menu` VALUES ('7150bbd7766b1458fc40b4225a6ccc0b', '6', '设置角色菜单', 'el-icon-s-tools', 'SYS_SET_MENU', '', '2', 4);
INSERT INTO `sys_menu` VALUES ('7f6368cdcf8dae04febe1c01d3a7651b', '0d237ca74ad890c300a224902d1ae880', '字典项新增', 'el-icon-plus', 'SYS_DICT_ITEM_ADD', '', '2', 4);
INSERT INTO `sys_menu` VALUES ('8', '6', '角色新增', 'el-icon-plus', 'SYS_ROLE_ADD', '', '2', 2);
INSERT INTO `sys_menu` VALUES ('8183c0c5d3e344391db8ac90a23333de', '2', '设置角色', 'el-icon-s-tools', 'SYS_USER_SET_ROLE', '', '2', 5);
INSERT INTO `sys_menu` VALUES ('9', '6', '角色删除', 'el-icon-delete', 'SYS_ROLE_DEL', '', '2', 3);
INSERT INTO `sys_menu` VALUES ('aaba448cc76a70718ef4ea0417503d8c', '0d237ca74ad890c300a224902d1ae880', '字典删除', 'el-icon-delete', 'SYS_DICT_DEL', '', '2', 3);
INSERT INTO `sys_menu` VALUES ('d2bcf381ad24ff724c76f8afef7bf3cb', '0d237ca74ad890c300a224902d1ae880', '字典项删除', 'el-icon-delete', 'SYS_DICT_ITEM_DEL', '', '2', 6);
INSERT INTO `sys_menu` VALUES ('ed7eed436e6c5a1e86c0df021096f8a8', '0d237ca74ad890c300a224902d1ae880', '字典编辑', 'el-icon-edit', 'SYS_DICT_UPDATE', '', '2', 2);

-- ----------------------------
-- Table structure for sys_menu_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu_role`;
CREATE TABLE `sys_menu_role`  (
  `id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  `role_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '权限id',
  `menu_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '菜单id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '权限菜单表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_menu_role
-- ----------------------------
INSERT INTO `sys_menu_role` VALUES ('04101214e8bac363fc6d2cae230e0937', '1', '8');
INSERT INTO `sys_menu_role` VALUES ('052572587dcfca4928a5fad74e873631', '1b82d1c7556352cf84bfdfe8248e8ad1', '5');
INSERT INTO `sys_menu_role` VALUES ('05a7f8a3d99ef118bd360da09271f7e3', '1b82d1c7556352cf84bfdfe8248e8ad1', '8183c0c5d3e344391db8ac90a23333de');
INSERT INTO `sys_menu_role` VALUES ('074e6c5acde818d791c67e3ce2a006a8', '1', '3131b3aeb78f2c9f29009c23b1afe963');
INSERT INTO `sys_menu_role` VALUES ('0b7d6eaac1939e4deec96c098438693c', 'b243975d18bf1728bd39ad22e22dbea2', '10');
INSERT INTO `sys_menu_role` VALUES ('103b8a93f33468e5a890b455d9c2befc', 'b243975d18bf1728bd39ad22e22dbea2', '2');
INSERT INTO `sys_menu_role` VALUES ('1bbff24522840ce6473c80ec840149d9', '1b82d1c7556352cf84bfdfe8248e8ad1', '2');
INSERT INTO `sys_menu_role` VALUES ('1f6420dde3e68f61da0cdf505cb736bf', '2', '10');
INSERT INTO `sys_menu_role` VALUES ('2b69a681f7aa29285cdae9fa9648fbb2', '1b82d1c7556352cf84bfdfe8248e8ad1', '4');
INSERT INTO `sys_menu_role` VALUES ('364fc4fe689213e73072bf2cbc1209f2', 'b243975d18bf1728bd39ad22e22dbea2', '0d237ca74ad890c300a224902d1ae880');
INSERT INTO `sys_menu_role` VALUES ('3871833c375b887ebba42d9b5b374492', '1', 'ed7eed436e6c5a1e86c0df021096f8a8');
INSERT INTO `sys_menu_role` VALUES ('48ecd14ef84b6f6fc34ddf303bae620c', '1', '13');
INSERT INTO `sys_menu_role` VALUES ('48fcece2c3972b7a3b80e2e1fad6c772', '1', '3');
INSERT INTO `sys_menu_role` VALUES ('4b5d417f4a9f54ea5b9c4dab75d0c002', '1', '10');
INSERT INTO `sys_menu_role` VALUES ('4d48583bcb9a06cbe148ea43810f7d6a', '1b82d1c7556352cf84bfdfe8248e8ad1', '3');
INSERT INTO `sys_menu_role` VALUES ('5440ddddb4d7d6ea782c5cb70b457824', '1', '11');
INSERT INTO `sys_menu_role` VALUES ('54a3611930fcfa94d355d9383d2dd369', '1', '8183c0c5d3e344391db8ac90a23333de');
INSERT INTO `sys_menu_role` VALUES ('62bf03f9ed63f7ed2d91ef20ce3022c1', '1', '7f6368cdcf8dae04febe1c01d3a7651b');
INSERT INTO `sys_menu_role` VALUES ('6722bd0a5fce2f77ceef7ce9353b911d', '2', '1');
INSERT INTO `sys_menu_role` VALUES ('738b663e074b418a2ed303e935234cb6', '1', '6a894f406313aaad9be5745b26d9027c');
INSERT INTO `sys_menu_role` VALUES ('782df1d4be42355c08ef0e143064a0dc', '1', '4c6e74acf214c843a0526bbc566bedd8');
INSERT INTO `sys_menu_role` VALUES ('808acdb00085cb9f3dc7ce9eeb849533', '1', '4');
INSERT INTO `sys_menu_role` VALUES ('8d99b93e04b204f1d1f0e74f60a6eaf7', '1', '7');
INSERT INTO `sys_menu_role` VALUES ('8f3d19cccbd5407f23d9af3e81414940', '1', '0d237ca74ad890c300a224902d1ae880');
INSERT INTO `sys_menu_role` VALUES ('93cfcfcb96e65a27f0a1ccd5b1785742', '1', 'd2bcf381ad24ff724c76f8afef7bf3cb');
INSERT INTO `sys_menu_role` VALUES ('a26714021c6cde9f6c80ba86b17e6a9a', '1', '5e2db6fa3eebbad27de5eaf97e7430ba');
INSERT INTO `sys_menu_role` VALUES ('a38b5a7ca8c5146ef82fa1cc5b9cb3bc', 'b243975d18bf1728bd39ad22e22dbea2', '1');
INSERT INTO `sys_menu_role` VALUES ('be2b3883e16d4f91227e900c39092a79', '1', '6');
INSERT INTO `sys_menu_role` VALUES ('c5b7164fdeb197e3b1671745a4da8553', '1', '12');
INSERT INTO `sys_menu_role` VALUES ('c991c59ee899528d74d2ab7e76ec8acb', '1b82d1c7556352cf84bfdfe8248e8ad1', '3131b3aeb78f2c9f29009c23b1afe963');
INSERT INTO `sys_menu_role` VALUES ('dbe8aafcc5538cfb8defa217860f6dce', '1b82d1c7556352cf84bfdfe8248e8ad1', '1');
INSERT INTO `sys_menu_role` VALUES ('dd2c3c25b7d795376b73b9041551e2c8', '1', '1');
INSERT INTO `sys_menu_role` VALUES ('df4dc405808eb812ebb9109d066a7e56', '1', '2');
INSERT INTO `sys_menu_role` VALUES ('e343fb7c4a5b8f295dba93a0e6cb81c0', '1', '5');
INSERT INTO `sys_menu_role` VALUES ('fa6a3ef837dac9348f03f757a9b42178', '1', 'aaba448cc76a70718ef4ea0417503d8c');
INSERT INTO `sys_menu_role` VALUES ('fb4b69fcfebf2bf839675e9948e6ecd1', '1', '7150bbd7766b1458fc40b4225a6ccc0b');
INSERT INTO `sys_menu_role` VALUES ('fe4770826be16857170d806eb0fd2aaf', '1', '9');
INSERT INTO `sys_menu_role` VALUES ('fe8c9c0233fc5190c3efabaca7a0a5a0', 'b243975d18bf1728bd39ad22e22dbea2', '6');

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色表主键',
  `code` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '角色编码',
  `name` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '角色名',
  `create_date` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `create_user` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `update_date` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `update_user` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `code`(`code`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES ('1', 'ADMIN', '管理员', '2021-05-15 00:51:54', '1', NULL, NULL);
INSERT INTO `sys_role` VALUES ('b243975d18bf1728bd39ad22e22dbea2', 'USER', '用户', '2021-06-21 15:51:51', '1', NULL, NULL);

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  `username` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户名',
  `password` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '密码',
  `name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '昵称',
  `create_date` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `create_user` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `update_date` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `update_user` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `state` char(2) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '字段状态：01:启用：02：禁用  -关联码表state',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES ('1', 'admin', '$2a$10$lAqcKF0CLMHzh2rBgqB/HeVNlrGKfXhbZfZAQq3va6u9JE4HLQh56', '管理员', '2021-05-17 17:58:51', NULL, '2021-08-01 10:18:20', NULL, '01');
INSERT INTO `sys_user` VALUES ('2e04f7d029d301f569f1276824cdc0fd', 'zhangsan', '$2a$10$pEe9DGXjxzz9MXMfr24gEuefx9FKJJYWWqOW/MotwDMOI3DI9U9Bi', '张三', '2021-06-21 15:48:42', '1', '2021-09-14 17:50:19', '1', '01');
INSERT INTO `sys_user` VALUES ('43a92f05e1b29db2ced99358dd89a590', 'wangwu', '$2a$10$LsRN/tNErgqdiv/0NI.VGOpwtF9hqrQB0mpfGSx5IMaZ7lptaEk6q', '王五', '2021-09-17 11:19:52', '1', '2021-09-17 11:20:54', '1', '01');

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role`  (
  `id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户角色表',
  `role_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '角色id',
  `user_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户角色表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES ('219751da8d211e1a20a49ba3302afc70', 'b243975d18bf1728bd39ad22e22dbea2', '2e04f7d029d301f569f1276824cdc0fd');
INSERT INTO `sys_user_role` VALUES ('3001f0fb354b4f12d9cab27270373c1f', '1', '1');
INSERT INTO `sys_user_role` VALUES ('47184e965ab6fee7cadd35208a71ac72', '1', '4a27f333630401ce1387ac50537d39b6');
INSERT INTO `sys_user_role` VALUES ('5563757ced0d56ed847c36e4055dbc30', '2', 'cebd85f887213b24ff2c14c437760c75');
INSERT INTO `sys_user_role` VALUES ('662117786d7f72507e93a8d7af058dcd', 'b243975d18bf1728bd39ad22e22dbea2', '4a27f333630401ce1387ac50537d39b6');
INSERT INTO `sys_user_role` VALUES ('6b22fd8d27a1692925d43e4b116915b6', '1', '43a92f05e1b29db2ced99358dd89a590');
INSERT INTO `sys_user_role` VALUES ('91f3c4084f6f62abdcf42640c97b0e3b', '3', 'cebd85f887213b24ff2c14c437760c75');
INSERT INTO `sys_user_role` VALUES ('ca3eb24792500876960edb84e30d7a61', 'b243975d18bf1728bd39ad22e22dbea2', '43a92f05e1b29db2ced99358dd89a590');
INSERT INTO `sys_user_role` VALUES ('d0d90a54a8ccad6ea0b3a084648346c8', '1', '77835a0841d652cef7abf8b5a5632532');

-- ----------------------------
-- Table structure for sys_wx_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_wx_user`;
CREATE TABLE `sys_wx_user`  (
  `id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键id',
  `nick_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '微信名称',
  `avatar_url` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '微信头像',
  `open_id` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '微信小程序openID',
  `union_id` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户在开放平台的唯一标识符',
  `raw_data` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户非敏感信息',
  `signature` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '签名',
  `encrypted_data` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户敏感信息',
  `iv` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '解密算法的向量',
  `create_date` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_wx_user
-- ----------------------------
INSERT INTO `sys_wx_user` VALUES ('1510bb904bfd780beb4a332064feed32', '佳伦不是桂伦', 'http://tmp/hXFqDUXiyKqy9f1093ce90dd3b23d634af276792ef84.jpeg', 'oVd1u5YjzftD7VgW5MQfTOZvI4sk', NULL, '{\"nickName\":\"微信用户\",\"gender\":0,\"language\":\"\",\"city\":\"\",\"province\":\"\",\"country\":\"\",\"avatarUrl\":\"https://thirdwx.qlogo.cn/mmopen/vi_32/POgEwh4mIHO4nibH0KlMECNjjGxQUq24ZEaGT4poC6icRiccVGKSyXwibcPq4BWmiaIGuG1icwxaQX6grC9VemZoJ8rg/132\",\"is_demote\":true}', 'f9cd5165dcb766d74427751aeffafddef41d3588', 'Hjqu6Tn2nqyQxsusLiDKcKBppxQSHf9ys7vcHSVOzAQmPHSb/griz9RLFkBj2zN0C+31+toqvJeqo2z3Jpek9lFfNco4oe/9fnOVnxHqiMx0/+2LIJamAHc0t0HqEK2Ijzx5CDr93dFYrlvyiGRRlj7B2CP9rSz8DK4j7kxED2StrmiEnSrRDBuYVmaVnAw9a1STMZTHlf5nMrC1jCXCnWekUKbCfexnNvbTcd4J3IK8i+w6iEUm2R5VLFs6xs3T1hXBCmCf95ipBzUdINweva403eBjuAwStu+6V3bj95huinVcP45YamhFn/nWlZk2BZOgFRJYyar8j6jYbGGRvsXv1+2KH11s1C17+mgIpAOXeOmTTyLCEqsJ5SszsITXqHUDH3GkbJ2S1peX7sPlRF5g+ewJVSeRNuzo6NS9jK0e7KhBfYQ4ZyG5fhArpggf', 'OQ3RlA/epATB8xPLiZKObQ==', '2024-11-13 15:00:18');

SET FOREIGN_KEY_CHECKS = 1;
