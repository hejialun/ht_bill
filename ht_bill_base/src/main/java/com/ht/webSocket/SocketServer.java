package com.ht.webSocket;

import cn.hutool.log.Log;
import cn.hutool.log.LogFactory;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.ht.util.JsonResult;
import org.springframework.stereotype.Component;

import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @ProjectName: ht
 * @ClassName: SocketServer
 * @Author: hejialun
 * @Description:
 * @Date: 2021/9/16 11:21
 */
@ServerEndpoint("/socketServer/{type}/{id}")
@Component
public class SocketServer {
    static Log log= LogFactory.get(SocketServer.class);
    //静态变量，用来记录当前在线连接数。应该把它设计成线程安全的
    private static int onlineCount = 0;
    //concurrent包的线程安全Set，用来存放每个客户端对应的MyWebSocket对象。
    private static ConcurrentHashMap<String,SocketServer> webSocketMap = new ConcurrentHashMap<>();
    //与某个客户端的连接会话，需要通过它来给客户端发送数据
    private Session session;
    //消息类型
    private String type;
    //接收消息的id
    private String id;
    //消息的key值-type+":"+id
    private String key;




    /*
     * @param session
     * @param type：消息类型
     * @param id：连接id
     * @Author hejialun
     * @Description: TODO(连接建立成功调用的方法)
     * @date 2021/9/16 11:23
     * @returns void
     */
    @OnOpen
    public void onOpen(Session session,@PathParam("type") String type,@PathParam("id") String id) {
        this.session = session;
        this.id=id;
        this.type=type;
        this.key=type+":"+id;
        if(webSocketMap.containsKey(key)){
            webSocketMap.remove(key);
            webSocketMap.put(key,this);
            //加入set中
        }else{
            webSocketMap.put(key,this);
            //加入set中
            addOnlineCount();
            //在线数加1
        }

        log.info("连接值:"+key+",当前在线人数为:" + getOnlineCount());

        try {
            sendMessage("连接成功");
        } catch (IOException e) {
            log.error("连接:"+key+",网络异常!!!!!!");
        }
    }

    /**
     * 连接关闭调用的方法
     */
    @OnClose
    public void onClose() {
        if(webSocketMap.containsKey(key)){
            webSocketMap.remove(key);
            //从set中删除
            subOnlineCount();
        }
        log.info("连接退出:"+key+",当前在线人数为:" + getOnlineCount());
    }

    /**
     * 收到客户端消息后调用的方法
     *
     * @param message 客户端发送过来的消息*/
    @OnMessage
    public void onMessage(String message, Session session) {
        log.info("连接:"+key+",报文:"+message);
        //可以群发消息
        //消息保存到数据库、redis
        if(StringUtils.isNotBlank(message)){
            try {
                //解析发送的报文
                JSONObject jsonObject = JSON.parseObject(message);
                //追加发送人(防止串改)
                jsonObject.put("type",this.type);
                jsonObject.put("id",this.id);
                //传送给对应toUserId用户的websocket
                if(StringUtils.isNotBlank(key)&&webSocketMap.containsKey(key)){
                    webSocketMap.get(key).sendMessage(jsonObject.toJSONString());
                }else{
                    log.error("请求的key:"+key+"不在该服务器上");
                    //否则不在这个服务器上，发送到mysql或者redis
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    /**
     *
     * @param session
     * @param error
     */
    @OnError
    public void onError(Session session, Throwable error) {
        log.error("连接错误:"+this.key+",原因:"+error.getMessage());
        error.printStackTrace();
    }

    /**
     * 实现服务器主动推送
     */
    public void sendMessage(String message) throws IOException {
        this.session.getBasicRemote().sendText(message);
        log.error("消息发送成功！");
    }

    /**
     * 发送指定key点发
     * */
    public static void sendInfo(Object object,@PathParam("key") String key) throws IOException {
        log.info("发送消息到:"+key+"，报文:"+object.toString());
        if(StringUtils.isNotBlank(key)&&webSocketMap.containsKey(key)){
            JsonResult success = JsonResult.success(object);
            webSocketMap.get(key).sendMessage(JSON.toJSONString(success));
        }else{
            log.error("连接"+key+",不在线！");
        }
    }


    /**
     * 发送指定type/群发
     * */
    public static void sendInfoType(Object object,@PathParam("type") String type) throws IOException {
        log.info("群发消息到:"+type+"，报文:"+object.toString());
        if(StringUtils.isNotBlank(type)){
            JsonResult success = JsonResult.success(object);
            //遍历map
            for (String key : webSocketMap.keySet()) {
                //判断key前缀是否是type开头的
                if(key.startsWith(type)){
                    //发送消息
                    webSocketMap.get(key).sendMessage(JSON.toJSONString(success));
                }
            }


        }
    }

    public static synchronized int getOnlineCount() {
        return onlineCount;
    }

    public static synchronized void addOnlineCount() {
        SocketServer.onlineCount++;
    }

    public static synchronized void subOnlineCount() {
        SocketServer.onlineCount--;
    }

}