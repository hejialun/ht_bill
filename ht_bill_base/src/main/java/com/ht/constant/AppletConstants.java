package com.ht.constant;

/**
 * @ClassName AppletConstants
 * @Description TODO（小程序常量）
 * @Author hejialun
 * @Date 2022/3/15 14:45
 * @Version 1.0
 */
public class AppletConstants {

    /**
     * 微信小程序appid
     */
    public static final String openId="openid";
    /**
     * 微信用户在开放平台的唯一标识符
     */
    public static final String unionId="unionid";
}
