package com.ht.constant;

import java.lang.reflect.Array;
import java.util.*;

/**
 * @ProjectName: ht
 * @ClassName: DicConstants
 * @Author: hejialun
 * @Description: 码表常量
 * @Date: 2021/9/14 11:02
 */
public class DicConstants {
    
    /**
     * 状态
     */
    public static class State {
        private State() {
            throw new UnsupportedOperationException();
        }
        //启用
        public static final String ENABLE = "01";
        //禁用
        public static final String DISABLE = "02";
    }


    /**
     * 菜单类型
     */
    public static class MenuType {
        private MenuType() {
            throw new UnsupportedOperationException();
        }
        //菜单
        public static final String MENU = "0";
        //页面
        public static final String PAGE = "1";
        //按钮
        public static final String BUTTON = "2";
    }


    /**
     * 消息类型
     */
    public static class SocketType{
        private SocketType() {
            throw new UnsupportedOperationException();
        }
        //用户消息
        public static final String USER = "user";
        //首页统计消息
        public static final String SY = "sy";
    }


}