package com.ht.module.statistics;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ht.constant.BusConstant;
import com.ht.module.bill.entity.BillRecord;
import com.ht.module.bill.mapper.BillRecordMapper;
import com.ht.module.sys.entity.SysWxUser;
import com.ht.module.sys.mapper.SysWxUserMapper;
import com.ht.util.JsonResult;
import com.ht.util.UserUtil;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @ClassName MeConstants
 * @Description TODO（）
 * @Author hejialun
 * @Date 2022/4/1 16:18
 * @Version 1.0
 */
@RestController
@RequestMapping("/statistics-me")
public class MeController {
    @Resource
    private SysWxUserMapper sysWxUserMapper;
    @Resource
    private BillRecordMapper billRecordMapper;


    @ApiOperation("使用统计")
    @GetMapping("/use")
    public JsonResult use(){
        Map<String,Object> map=new HashMap<>();
        String userId = UserUtil.getUserId();
        //使用天数
        SysWxUser wxUser = sysWxUserMapper.selectById(userId);
        Duration duration = Duration.between(wxUser.getCreateDate(),LocalDateTime.now());
        long days = duration.toDays(); //相差的天数
        //设置使用天数
        map.put("days",days);
        //设置记账天数
        List<BillRecord> records = billRecordMapper.findList(
                new QueryWrapper<BillRecord>()
                        .eq("create_user", userId)
        );

        int size = records.stream().collect(Collectors.groupingBy(BillRecord::getYearMonthDay)).keySet().size();
        //设置使用天数
        map.put("useDays",size);
        //设置记账次数
        map.put("useNum",records.size());
        //获取用户姓名和头像
        map.put("nickName",wxUser.getNickName());
        map.put("avatarUrl",wxUser.getAvatarUrl());
        return JsonResult.success(map);

    }
}
