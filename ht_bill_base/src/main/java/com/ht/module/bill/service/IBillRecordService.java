package com.ht.module.bill.service;

import com.ht.module.bill.entity.BillRecord;
import com.baomidou.mybatisplus.extension.service.IService;
import com.ht.util.Pager;

import java.math.BigDecimal;
import java.util.List;

/**
 * <p>
 * 消费记录表 服务类
 * </p>
 *
 * @author hejialun
 * @since 2022-03-17
 */
public interface IBillRecordService extends IService<BillRecord> {

    void add(BillRecord en);

    void delById(String id);

    Pager<BillRecord> findPageByMe(Pager<BillRecord> pager, BillRecord en);

    BigDecimal getMonthConsumption(Integer year,Integer month);

    List<BillRecord> findByMe(BillRecord en);
}
