package com.ht.module.bill.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import lombok.Data;
import lombok.experimental.Accessors;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.time.LocalDateTime;
import java.io.Serializable;


/**
 * <p>
 * 消费分类表
 * </p>
 *
 * @author hejialun
 * @since 2022-03-17
 */

@TableName("bill_clazz")
@Data
@Accessors(chain = true)
public class BillClazz{
    /**
     * 账单分类表组件
     */
    @TableId
    private String id;
    /**
     * 分类名称
     */
    private String name;
    /**
     * 分类图标
     */
    private String icon;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 创建时间
     */
    private LocalDateTime createDate;
    /**
     * 创建人
     */
    private String createUser;
    /**
     * 修改时间
     */
    private LocalDateTime updateDate;
    /**
     * 修改人
     */
    private String updateUser;
    /**
     * 逻辑删除
     */
    @TableLogic
    private Integer delFlag;


}
