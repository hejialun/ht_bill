package com.ht.module.bill.controller;

import com.ht.module.bill.service.IBillRecordService;
import com.ht.util.CommMethod;
import com.ht.util.JsonResult;
import com.ht.util.Pager;
import com.ht.util.UserUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.ht.module.bill.entity.BillRecord;

import javax.validation.Valid;


/**
 * <p>
 * 消费记录表 前端控制器
 * </p>
 *
 * @author hejialun
 * @since 2022-03-17
 */
@RestController
@RequestMapping("/bill-record")
public class BillRecordController {

    
    @Autowired
    private IBillRecordService iBillRecordService;

    


    /**
    * 获取本月消费
    */
    @GetMapping("/getMonthConsumption/{year}/{month}")
    public JsonResult getMonthConsumption(@PathVariable Integer year,@PathVariable Integer month){
        return JsonResult.success(iBillRecordService.getMonthConsumption(year,month));
    }

    /**
     * 列表查询自己的消费记录
     */
    @GetMapping("/findByMe")
    public JsonResult findByMe(BillRecord en){
        return JsonResult.success(iBillRecordService.findByMe(en));
    }


    /**
    * 分页自己的消费记录查询
    */
    @GetMapping("/findPageByMe")
    public JsonResult findPageByMe(Pager<BillRecord> pager, BillRecord en){
        return JsonResult.success(iBillRecordService.findPageByMe(pager,en));
    }


    /**
    * 通过id查询
    */
    @GetMapping("/get-by-id/{id}")
    public JsonResult getById(@PathVariable(value = "id") String id){
        return JsonResult.success(iBillRecordService.getById(id));
    }

    /**
    * 新增
    */
    @PostMapping("/add")
    public JsonResult add(@RequestBody @Valid BillRecord en){
        iBillRecordService.add(en);
        return JsonResult.success();
    }

    /**
    * 通过id删除
    */
    @GetMapping("/delete-by-id/{id}")
    public JsonResult delete(@PathVariable(value = "id") String id){
        iBillRecordService.delById(id);
        return JsonResult.success();
    }

    /**
    * 修改
    */
    @PostMapping("/update")
    public JsonResult updateById(@RequestBody @Valid BillRecord en){
        iBillRecordService.updateById(en);
        return JsonResult.success();
    }

}
