package com.ht.module.bill.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.ht.module.bill.entity.BillRecord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ht.util.Pager;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 消费记录表 Mapper 接口
 * </p>
 *
 * @author hejialun
 * @since 2022-03-17
 */
public interface BillRecordMapper extends BaseMapper<BillRecord> {


    Pager<BillRecord> findPage( Pager<BillRecord> pager,@Param(Constants.WRAPPER) QueryWrapper<BillRecord> qw);

    List<BillRecord> findList(@Param(Constants.WRAPPER) QueryWrapper<BillRecord> qw);
}
