package com.ht.module.bill.mapper;

import com.ht.module.bill.entity.BillClazz;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 消费分类表 Mapper 接口
 * </p>
 *
 * @author hejialun
 * @since 2022-03-17
 */
public interface BillClazzMapper extends BaseMapper<BillClazz> {

}
