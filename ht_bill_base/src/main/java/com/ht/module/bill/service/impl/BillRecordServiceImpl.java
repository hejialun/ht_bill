package com.ht.module.bill.service.impl;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ht.abnormal.HtException;
import com.ht.constant.BusConstant;
import com.ht.module.bill.entity.BillRecord;
import com.ht.module.bill.mapper.BillRecordMapper;
import com.ht.module.bill.service.IBillRecordService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ht.util.CommMethod;
import com.ht.util.Pager;
import com.ht.util.UserUtil;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

/**
 * <p>
 * 消费记录表 服务实现类
 * </p>
 *
 * @author hejialun
 * @since 2022-03-17
 */
@Service
public class BillRecordServiceImpl extends ServiceImpl<BillRecordMapper, BillRecord> implements IBillRecordService {

    @Override
    public void add(BillRecord en) {
        CommMethod.beanCreate(en);
        baseMapper.insert(en);
    }

    @Override
    public void delById(String id) {
        //获取当前
        BillRecord record = baseMapper.selectById(id);
        if(!record.getCreateUser().equals(UserUtil.getUserId())){
            throw new HtException("没有权限删除当前消费记录！");
        }
        baseMapper.deleteById(id);
    }

    @Override
    public Pager<BillRecord> findPageByMe(Pager<BillRecord> pager, BillRecord en) {
        QueryWrapper<BillRecord> qw=new QueryWrapper<>();
        if(StrUtil.isNotEmpty(en.getBillClazz())){
            qw.eq("tab.bill_clazz",en.getBillClazz());
        }
        if(ObjectUtil.isNotEmpty(en.getYear())){
            qw.eq("tab.year",en.getYear());
        }else{
            //获取当前年
            qw.eq("tab.year",LocalDate.now().getYear());
        }
        if(ObjectUtil.isNotEmpty(en.getMonth())){
            qw.eq("tab.month",en.getMonth());
        }else{
            qw.eq("tab.month",LocalDate.now().getMonth());
        }
        qw.eq("create_user",UserUtil.getUserId());
        qw.orderByDesc("create_date");
        return baseMapper.findPage(pager,qw);

    }

    @Override
    public BigDecimal getMonthConsumption(Integer year,Integer month) {
        QueryWrapper<BillRecord> qw=new QueryWrapper<>();
        qw.eq("tab.year",year);
        qw.eq("tab.month",month);
        qw.eq("tab.create_user",UserUtil.getUserId());
        List<BillRecord> list = baseMapper.findList(qw);
        //合计金额
        BigDecimal total = list.stream().map(BillRecord::getMoney).reduce(BigDecimal.ZERO, BigDecimal::add);
        return total;
    }

    @Override
    public List<BillRecord> findByMe(BillRecord en) {
        QueryWrapper<BillRecord> qw=new QueryWrapper<>();
        if(StrUtil.isNotEmpty(en.getBillClazz())){
            qw.eq("tab.bill_clazz",en.getBillClazz());
        }
        if(ObjectUtil.isNotEmpty(en.getYear())){
            qw.eq("tab.year",en.getYear());
        }else{
            //获取当前年
            qw.eq("tab.year",LocalDate.now().getYear());
        }
        if(ObjectUtil.isNotEmpty(en.getMonth())){
            qw.eq("tab.month",en.getMonth());
        }else{
            qw.eq("tab.month",LocalDate.now().getMonth());
        }

        qw.eq("create_user",UserUtil.getUserId());
        qw.orderByDesc("bill_date");
        return  baseMapper.findList(qw);
    }

}
