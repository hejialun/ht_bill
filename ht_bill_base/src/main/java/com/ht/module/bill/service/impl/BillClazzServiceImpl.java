package com.ht.module.bill.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ht.module.bill.entity.BillClazz;
import com.ht.module.bill.mapper.BillClazzMapper;
import com.ht.module.bill.service.IBillClazzService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 消费分类表 服务实现类
 * </p>
 *
 * @author hejialun
 * @since 2022-03-17
 */
@Service
public class BillClazzServiceImpl extends ServiceImpl<BillClazzMapper, BillClazz> implements IBillClazzService {

    @Override
    public List<BillClazz> findList(BillClazz en) {
        QueryWrapper<BillClazz> qw=new QueryWrapper<>();
        qw.orderByAsc("sort");
        return baseMapper.selectList(qw);
    }
}
