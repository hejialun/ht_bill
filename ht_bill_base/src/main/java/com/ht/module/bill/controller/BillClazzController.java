package com.ht.module.bill.controller;

import com.ht.module.bill.service.IBillClazzService;
import com.ht.util.JsonResult;
import com.ht.util.Pager;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.ht.module.bill.entity.BillClazz;

import javax.validation.Valid;


/**
 * <p>
 * 消费分类表 前端控制器
 * </p>
 *
 * @author hejialun
 * @since 2022-03-17
 */
@RestController
@RequestMapping("/bill-clazz")
public class BillClazzController {

    
    @Autowired
    private IBillClazzService iBillClazzService;

    @ApiOperation("查询分类")
    @GetMapping("/findList")
    public JsonResult findList( BillClazz en){
        return JsonResult.success(iBillClazzService.findList(en));
    }


    /**
    * 分页查询
    */
    @GetMapping("/findPage")
    public JsonResult findPage(Pager<BillClazz> pager, BillClazz en){
        return JsonResult.success(iBillClazzService.page(pager));
    }


    /**
    * 通过id查询
    */
    @GetMapping("/get-by-id/{id}")
    public JsonResult getById(@PathVariable(value = "id") String id){
        return JsonResult.success(iBillClazzService.getById(id));
    }

    /**
    * 新增
    */
    @PostMapping("/add")
    public JsonResult add(@RequestBody @Valid BillClazz en){
        iBillClazzService.save(en);
        return JsonResult.success();
    }

    /**
    * 通过id删除
    */
    @DeleteMapping("/delete-by-id/{id}")
    public JsonResult delete(@PathVariable(value = "id") String id){
        iBillClazzService.removeById(id);
        return JsonResult.success();
    }

    /**
    * 修改
    */
    @PutMapping("/update")
    public JsonResult updateById(@RequestBody @Valid BillClazz en){
        iBillClazzService.updateById(en);
        return JsonResult.success();
    }

}
