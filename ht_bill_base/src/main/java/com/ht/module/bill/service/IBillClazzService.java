package com.ht.module.bill.service;

import com.ht.module.bill.entity.BillClazz;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 消费分类表 服务类
 * </p>
 *
 * @author hejialun
 * @since 2022-03-17
 */
public interface IBillClazzService extends IService<BillClazz> {

    List<BillClazz> findList(BillClazz en);
}
