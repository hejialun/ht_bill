package com.ht.module.bill.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import lombok.Data;
import lombok.experimental.Accessors;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.io.Serializable;


/**
 * <p>
 * 消费记录表
 * </p>
 *
 * @author hejialun
 * @since 2022-03-17
 */

@TableName("bill_record")
@Data
@Accessors(chain = true)
public class BillRecord{
    /**
     * 账单分类表组件
     */
    @TableId
    private String id;
    /**
     * 消费分类
     */
    @NotEmpty
    private String billClazz;
    /**
     * 消费金额
     */
    @NotNull
    private BigDecimal money;
    /**
     * 消费日期
     */
    @NotNull
    private LocalDate billDate;
    /**
     * 消费备注
     */
    private String note;
    /**
     * 创建时间
     */
    private LocalDateTime createDate;
    /**
     * 创建人
     */
    private String createUser;
    /**
     * 修改时间
     */
    private LocalDateTime updateDate;
    /**
     * 修改人
     */
    private String updateUser;

    /**
     * 年
     */
    @TableField(exist = false)
    private Integer year;

    /**
     * 月
     */
    @TableField(exist = false)
    private Integer month;

    /**
     * 年月
     */
    @TableField(exist = false)
    private String yearMonth;
    /**
     * 年月日
     */
    @TableField(exist = false)
    private String yearMonthDay;


}
