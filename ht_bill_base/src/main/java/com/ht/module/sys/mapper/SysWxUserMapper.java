package com.ht.module.sys.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ht.module.sys.entity.SysWxUser;

/**
 * <p>
 * 字典项表 Mapper 接口
 * </p>
 *
 * @author Auto-generator
 * @since 2021-05-19
 */
public interface SysWxUserMapper extends BaseMapper<SysWxUser> {

}
