package com.ht.module.sys.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.time.LocalDateTime;


/**
 * <p>
 * 字典表
 * </p>
 *
 * @author Auto-generator
 * @since 2021-05-19
 */

@TableName("sys_wx_user")
@Data
@Accessors(chain = true)
public class SysWxUser {

    /**
     * 主键
     */
    @TableId
    private String id;

    /**
     * 微信名称
     */
    private String nickName;
    /**
     * 微信头像
     */
    private String avatarUrl;

    /**
     * 微信小程序openID
     */
    private String openId;

    /**
     * 用户在开放平台的唯一标识符
     */
    private String unionId;

    /**
     * 用户非敏感信息
     */
    private String rawData;

    /**
     * 签名
     */
    private String signature;

    /**
     * 用户敏感信息
     */
    private String encryptedData;

    /**
     * 解密算法的向量
     */
    private String iv;

    /**
     * 创建时间
     */
    private LocalDateTime createDate;


    /**
     * 登录认证
     */
    @TableField(exist = false)
    private String code;

}
