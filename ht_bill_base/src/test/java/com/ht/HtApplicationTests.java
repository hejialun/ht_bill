package com.ht;

import cn.hutool.http.HttpUtil;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class HtApplicationTests {

    @Test
    void test(){
        for (int i = 0; i < 10; i++) {
            int finalI = i;
            new Thread(new Runnable() {
                @Override
                public void run() {
                    String str = HttpUtil.get("http://127.0.0.1:9000/redisson/" + finalI);
                    System.err.println(str);
                }
            }).start();
        }
        while (true){

        }

    }

}
