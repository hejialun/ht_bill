var vue = new Vue({})



/**
 * 深度合并对象
 * @param obj1
 * @param obj2
 * @returns {*}
 */
function deepMerge(obj1, obj2) {
    let key;
    for (key in obj2) {
        obj1[key] =
            obj1[key] &&
            obj1[key].toString() === "[object Object]" &&
            (obj2[key] && obj2[key].toString() === "[object Object]")
                ? deepMerge(obj1[key], obj2[key])
                : (obj1[key] = obj2[key]);
    }
    return obj1;
}


function buttonPermissionsCheck(code) {
    let permissions=getSession("userInfo").permissions;
    //判断是否有当前权限
    return  permissions.filter(x=>x.code==code).length==0?false:true;
}


/**
 * ajax get方法
 * @param url:访问url
 * @param data:数据
 * @param fun:回调函数
 */
function get(url, data, fun) {
    $.ajax({
        url: url,
        data: data,
        success: function (res) {
            //判断是否异常
            if(res.code!=1){
                vue.$message.error(res.msg);
            }else{
                if(fun==null){
                    data(res.data);
                }else{
                    fun(res.data);
                }
            }
        }
    })
}

/**
 * ajax del请求
 * @param url:访问url
 * @param data:数据
 * @param fun:回调函数
 */
function del(url, data, fun) {
    $.ajax({
        url: url,
        type: "delete",
        data: data,
        success: function (res) {
            if(res.code!=1){
                vue.$message.error(res.msg);
            }else{
                if(fun==null){
                    data(res);
                }else{
                    fun(res);
                }
            }

        }
    })
}


/**
 * ajax post方法
 * @param url:访问url
 * @param data:数据
 * @param fun:回调函数
 */
function post(url, data, fun) {
    $.ajax({
        url: url,
        type: "post",
        data: data,
        success: function (res) {
            if(res.code!=1){
                vue.$message.error(res.msg);
            }else{
                if(fun==null){
                    data(res);
                }else{
                    fun(res);
                }
            }

        }
    })
}

function postJson(url, data, fun) {
    $.ajax({
        url: url,
        type: "POST",
        data: JSON.stringify(data),
        dataType: "json",
        contentType: "application/json",
        success: function (res) {
            if(res.code!=1){
                vue.$message.error(res.msg);
            }else{
                if(fun==null){
                    data(res);
                }else{
                    fun(res);
                }
            }

        }
    })
}



/**
 * ajax put方法
 * @param url:访问url
 * @param data:数据
 * @param fun:回调函数
 */
function put(url, data, fun) {
    $.ajax({
        url: url,
        type: "put",
        data: data,
        success: function (res) {
            if(res.code!=1){
                vue.$message.error(res.msg);
            }else{
                if(fun==null){
                    data(res);
                }else{
                    fun(res);
                }
            }

        }
    })
}


/**
 * 获取session数据
 * @param key:键
 */
function getSession(key){
    return JSON.parse(window.sessionStorage.getItem(key));
}

/**
 * 设置session数据
 * @param key：键
 * @param value：值
 */
function setSession(key,value) {
    window.sessionStorage.setItem(key, JSON.stringify(value));
}



/**
 * 获取登录用户并设置session
 */
function getLoginUser(){
    let ret;
    $.ajax({
        type : "get",
        url : "/sys-user/getLoginUser",
        async:false,  //使用同步的方式,true为异步方式
        dataType : "json",
        success : function(res) {
            setSession("userInfo",res.data)
            ret=res.data;
        }
    });
    return ret;
}