import Vue from 'vue'
import App from './App'
import request from 'common/request.js'
import config from 'common/config.js'
import {Tool} from 'common/tool.js'
import {DateUtil} from 'common/dateUtil.js'



// vuex
import store from './store'

// 引入全局uView
import uView from '@/uni_modules/uview-ui'

import mixin from './common/mixin'

//全局挂靠请求分装
Vue.prototype.$request = request
Vue.prototype.$config = config
Vue.prototype.$tool = new Tool();
Vue.prototype.$dateUtil = new DateUtil();

Vue.prototype.$store = store

Vue.config.productionTip = false

App.mpType = 'app'
Vue.use(uView)

// #ifdef MP
// 引入uView对小程序分享的mixin封装
const mpShare = require('@/uni_modules/uview-ui/libs/mixin/mpShare.js')
Vue.mixin(mpShare)
// #endif

Vue.mixin(mixin)

const app = new Vue({
    store,
    ...App
})

// 引入请求封装
require('./util/request/index')(app)

app.$mount()
