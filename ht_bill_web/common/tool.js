import config from 'common/config.js'


export class Tool {
    /**
     * 获取服务器资源
     * @param url:相对路径
     * @returns {string}
     */
    getResources(url) {
        return config.baseUrl + url + "?token=" + this.getToken();
    }

    /**
     * 获取token
     */
    getToken() {
        let token = uni.getStorageSync("token");
        return token;
    }
}




