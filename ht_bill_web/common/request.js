import config from 'common/config.js'
import {Tool} from 'common/tool.js'
const baseUrl = config.baseUrl;
const request = (url = '', date = {}, type = 'GET', header = {
}) => {
    return new Promise((resolve, reject) => {
        //获取token
        header=Object.assign(header,{
            "X-Access-Token":new Tool().getToken(),
        })
        uni.request({
            method: type,
            url: baseUrl + url,
            data: date,
            header: header,
            dataType: 'json',
        }).then((response) => {
            setTimeout(function() {
                uni.hideLoading();
            }, 200);
            let [error, res] = response;
            if(res.data.code==1){
                resolve(res.data.data);
            }else{
                uni.$u.toast(res.data.msg)
                reject();
            }
        }).catch(error => {
            let [err, res] = error;
            uni.$u.toast(err);
            reject(err)
        })
    });
}
export default request
