import Vue from 'vue'
import Vuex from 'vuex'
import request from 'common/request.js'

Vue.use(Vuex) // vue的插件机制

// Vuex.Store 构造器选项
const store = new Vuex.Store({
    // 为了不和页面或组件的data中的造成混淆，state中的变量前面建议加上$符号
    state: {
        //当前锁定的时间
        yearMonth: {
            year: "",
            month: "",
        },
        // 用户信息
        userInfo: null,
    },
    mutations: {
        /**
         * 登录
         * @param state
         * @param nickName
         * @param avatarUrl
         * @param fun
         */
        login(state, {nickName, avatarUrl, fun}) {
            var data = {}
            //获取用户信息
            wx.getUserProfile({
                desc: '用于完善会员资料', // 声明获取用户个人信息后的用途，后续会展示在弹窗中，请谨慎填写
                success: info => {
                    data["nickName"] = nickName;
                    data["avatarUrl"] = avatarUrl;
                    data["rawData"] = info.rawData;
                    data["signature"] = info.signature;
                    data["encryptedData"] = info.encryptedData;
                    data["iv"] = info.iv;
                    wx.login({
                        success: res => {
                            data["code"] = res.code;
                            if (res.code) {
                                //发起网络请求
                                request("/sys-login/wxLogin", JSON.stringify(data), "post").then(res => {
                                    uni.setStorageSync("token", res.token);
                                    state.userInfo = {
                                        nickName: nickName,
                                        avatarUrl: avatarUrl,
                                    };
                                    uni.$u.toast("登录成功")
                                    if (fun != null && fun !== "") {
                                        fun();
                                    }
                                }).catch(err => {
                                    uni.$u.toast("" + err.msg)
                                    //登录失败清楚token
                                    uni.removeStorageSync('token');
                                    uni.$u.toast(err.msg)
                                    //登录成功后回调
                                })
                            } else {
                                uni.$u.toast('登录失败！' + res.errMsg)
                            }

                        }
                    })

                }
            })
        },

        /**
         * 退出登录
         * @param state
         */
        logout(state) {
            //退出登录清理所有缓存
            uni.clearStorage()
            state.userInfo = null
        },

        //设置当前年月
        setYearMonth(state, yearMonth) {
            state.yearMonth = yearMonth;
        },

        /**
         * 设置用户信息
         * @param state
         * @param userInfo
         */
        setUserInfo(state, userInfo) {
            state.userInfo = userInfo;
        }
    },
    getters: {
        getUserInfo(state) {
            return state.userInfo;
        },

        //获取当前年月
        getYearMonth(state) {
            return state.yearMonth;
        }
    },

})

export default store
