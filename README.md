# 基于uniapp +springboot实现的记账微信小程序

#### 介绍
基于uniapp +springboot实现的记账微信小程序

#### 软件架构
    前端基于uni-app+uview
    后端是springboot


#### 安装教程
    mysql5.7
    node.js 16
    jdk8

#### 使用说明
    安装 HBuilder X
    安装微信开发者工具
    安装依赖：cnpm i 
    运行项目
    
    
#### 演示图
  个人页面
  ![info](/img/gr.jpg)
  
  登录后页面
  ![info](/img/login.jpg)
  
  流水页面
  ![info](/img/ls.jpg)
  ![info](/img/ls2.jpg)
  ![info](/img/ls3.jpg)
  
  记账页面
  ![info](/img/jl.jpg)
  
  编辑页面
  ![info](/img/bj.jpg)
